package com.com.xentinels.amc.client.ActividadDeUsuarios;

/**
 * Created by user on 30/08/2017. 30
 */

public class SolicitudesFragment {
    private String id;

    public SolicitudesFragment() {
    }

    public SolicitudesFragment(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
