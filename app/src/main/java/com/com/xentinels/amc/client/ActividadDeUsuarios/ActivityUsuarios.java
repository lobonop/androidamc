package com.com.xentinels.amc.client.ActividadDeUsuarios;
import android.content.Intent;
import android.os.Bundle;

import com.com.xentinels.amc.client.ActividadDeUsuarios.Cards.ListCardsActivity;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.com.xentinels.amc.client.Login;
import com.com.xentinels.amc.client.Preferences;
import com.xentinels.amc.clients.R;
import static java.util.Objects.requireNonNull;


public class ActivityUsuarios extends AppCompatActivity {

    private ViewPager viewPager;
    //private static final String TAG = "RegIntentService";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("");
        setContentView(R.layout.activity_home);
        TabLayout tabLayout = findViewById(R.id.tabLayoutUsuarios);
        viewPager = findViewById(R.id.viewPagerUsuarios);
        tabLayout.setupWithViewPager(viewPager);
        AdapterUsuarios adapter = new AdapterUsuarios(getSupportFragmentManager());
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //Toast.makeText(ActivityUsuarios.this, "tabSelected:  " + tab.getText(), Toast.LENGTH_SHORT).show();
                // no where in the code it is defined what will happen when tab is tapped/selected by the user
                // this is why the following line is necessary
                // we need to manually set the correct fragment when a tab is selected/tapped
                // and this is the problem in your code
                viewPager.setCurrentItem(tab.getPosition());
                Fragment fragment = ((AdapterUsuarios) requireNonNull(viewPager.getAdapter())).getFragment(tab.getPosition());

                if (fragment != null)
                    fragment.onResume();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                //Toast.makeText(ActivityUsuarios.this, "tabReSelected:  " + tab.getText(), Toast.LENGTH_SHORT).show();
                // Reload your recyclerView here
            }
        });



        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener( item -> {
            int id = item.getItemId();
            if (id == R.id.nav_list) {
                Intent intent = new Intent(ActivityUsuarios.this, ListCardsActivity.class);
                startActivity(intent);
            }else if (id == R.id.nav_close_session) {
                Preferences.savePreferenceBoolean(ActivityUsuarios.this,false,Preferences.PREFERENCE_SESSION_KEEP_LOGGED_IN);
//                    logoutToken();
                Intent i = new Intent(ActivityUsuarios.this,Login.class);
                startActivity(i);
                finish();
            }
            drawer.closeDrawer(GravityCompat.START);
            return true;
        });



        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position==0){
                    setTitle("Categorías disponibles");
                }else if(position==1){
                    setTitle("Mis Citas");
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        viewPager.setOffscreenPageLimit(3);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}

