package com.com.xentinels.amc.client.ActividadDeUsuarios.ClasesComunicacion;

/**
 * Created by user on 16/06/2017. 16
 */

/*id
nombreCompleto
apellido
estado
fecha_amigos
mensaje
hora_del_mensaje*/

public class Usuario {

    private String id;
    private String nombreCompleto;
    private int estado;
    private String mensaje;
    private String foto;

    public Usuario(){

    }

    public Usuario(String id, String nombreCompleto, int estado, String fotoPerfil) {
        this.id = id;
        this.nombreCompleto = nombreCompleto;
        this.estado = estado;
        foto = fotoPerfil;
    }

    public Usuario(String id, String nombreCompleto, String mensaje, String fotoPerfil) {
        this.id = id;
        this.nombreCompleto = nombreCompleto;
        this.mensaje = mensaje;
        foto = fotoPerfil;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setDescripcion(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String fotoPerfil) {
        foto = fotoPerfil;
    }
}
