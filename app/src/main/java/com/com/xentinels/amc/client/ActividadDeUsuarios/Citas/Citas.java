package com.com.xentinels.amc.client.ActividadDeUsuarios.Citas;

/**
 * Created by user on 19/05/2017.
 */

public class Citas {

    String code;
    String nombreServicio;
    String estado;
    String total;
    String horaInicio;
    String horaFin;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNombreServicio() {
        return nombreServicio;
    }

    public void setNombreServicio(String nombreServicio) {
        this.nombreServicio = nombreServicio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }



    public Citas(String code, String nombreServicio, String estado, String total, String horaInicio, String horaFin) {
        this.code = code;
        this.nombreServicio = nombreServicio;
        this.estado = estado;
        this.total = total;
        this.horaInicio = horaInicio;
        this.horaFin = horaFin;
    }



}
