package com.com.xentinels.amc.client;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;


import com.andreabaccega.formedittextvalidator.RegexpValidator;
import com.andreabaccega.widget.FormEditText;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.com.xentinels.amc.client.Internet.SolicitudesJson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import com.xentinels.amc.clients.R;

public class Registro extends AppCompatActivity {

    private FormEditText email;
    private FormEditText password;
    private FormEditText name;
    private FormEditText lastname;
    private FormEditText phone_number;
    private Button registro;

    private FormEditText day;
    private FormEditText month;
    private FormEditText year;

    private RadioButton rdHombre;
    private RadioButton rdMujer;

    private VolleyRP volley;
    private RequestQueue mRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        volley = VolleyRP.getInstance(this);
        mRequest = volley.getRequestQueue();
        Toolbar toolbar = findViewById(R.id.toolbarRegistro);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Registro.this.onBackPressed();
            }
        });
        email = findViewById(R.id.emailRegister);
        password = findViewById(R.id.passwordRegistro);
        name = findViewById(R.id.nameRegistro);
        lastname = findViewById(R.id.lastnameRegistro);
        day = findViewById(R.id.dayRegistro);
        month = findViewById(R.id.monthRegistro);
        year = findViewById(R.id.yearRegistro);
        phone_number = findViewById(R.id.phoneRegistro);
        password.addValidator(new RegexpValidator("Not a valid UT EID","^(?=.*[0-9])(?=.*[a-zA-Z])(?=\\S+$).{6,20}$"));

        rdHombre = findViewById(R.id.RDhombre);
        rdMujer = findViewById(R.id.RDmujer);

        rdHombre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rdMujer.setChecked(false);
            }
        });

        rdMujer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rdHombre.setChecked(false);
            }
        });

        registro = findViewById(R.id.buttonRegistro);

        registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate()){
                String genre = "";

                if (rdHombre.isChecked()) genre = "MALE";
                else if (rdMujer.isChecked()) genre = "FEMALE";

                    Signup(
                        getStringET(email).trim(),
                        getStringET(password).trim(),
                        getStringET(name).trim(),
                        getStringET(lastname).trim(),
                        getStringET(year) + "-" + getStringET(month) + "-" + getStringET(day).trim(),
                        getStringET(phone_number).trim());
            }}
        });
    }

    private void Signup(final String email, String password, String name, String lastname, String borndate, final String phone){

            HashMap<String, String> SignupParams = new HashMap<>();
            SignupParams.put("username", email);
            SignupParams.put("password", password);
            SignupParams.put("first_name", name);
            SignupParams.put("last_name", lastname);
            SignupParams.put("phone", phone);
            SignupParams.put("is_provider", "false");
            SignupParams.put("email", email);

        JsonObjectRequest solicitud = new JsonObjectRequest(Request.Method.POST, SolicitudesJson.URL_SINGUP,new JSONObject(SignupParams), new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject datos) {
                try {
                    String token = datos.getString("token");
                    Toast.makeText(Registro.this,"el token es: "+token,Toast.LENGTH_LONG).show();
                    Preferences.savePreferenceString(Registro.this,token,Preferences.PREFERENCE_SESSION_TOKEN);
                    String phoneNumber = "+593" + phone;

                    Intent intent = new Intent(Registro.this, VerifyPhoneActivity.class);
                    intent.putExtra("phonenumber", phoneNumber);
                    startActivity(intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String jsonError = "";
                if (networkResponse != null && networkResponse.data != null) {
                    jsonError = new String(networkResponse.data);
                    System.out.println(jsonError);
                    VolleyLog.d("Error:", jsonError);
                    Toast.makeText(Registro.this,"El usuario o el teléfono ya existen", Toast.LENGTH_LONG).show();
                }
            }}){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        VolleyRP.addToQueue(solicitud,mRequest,this,volley);
    }

    private boolean validate() {
        FormEditText[] allFields={password, name, email, lastname,day,month,year,phone_number};
        boolean allValid = true;
        for (FormEditText field : allFields) {
            allValid = field.testValidity() && allValid;
        }
        return allValid;
    }

    private String getStringET(EditText e){
        return e.getText().toString();
    }
}
