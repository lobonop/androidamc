package com.com.xentinels.amc.client.ActividadDeUsuarios.Doctores.Servicios.ui.doctores;

import java.util.HashMap;

public class AtributosDoctores {

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBorn_date() {
        return born_date;
    }

    public void setBorn_date(String born_date) {
        this.born_date = born_date;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getCollege_name() {
        return college_name;
    }

    public void setCollege_name(String college_name) {
        this.college_name = college_name;
    }

    public String getCareer_name() {
        return career_name;
    }

    public void setCareer_name(String career_name) {
        this.career_name = career_name;
    }

    public HashMap<String, String> getSchedules() {
        return schedules;
    }

    public void setSchedules(HashMap<String, String> schedules) {
        this.schedules = schedules;
    }

    private String id;

    private String name;
    private String lastname;
    private String email;
    private String phone;
    private String gender;
    private String born_date;
    private String imageURL;
    private String identification;
    private String college_name;
    private String career_name;
    private HashMap<String,String> schedules;

    public AtributosDoctores(String id, String name, String lastname, String email, String phone, String gender, String born_date, String imageURL, String identification, String college_name, String career_name, HashMap<String, String> schedules) {
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.email = email;
        this.phone = phone;
        this.gender = gender;
        this.born_date = born_date;
        this.imageURL = imageURL;
        this.identification = identification;
        this.college_name = college_name;
        this.career_name = career_name;
        this.schedules = schedules;
    }

}
