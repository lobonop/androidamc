package com.com.xentinels.amc.client.ActividadDeUsuarios.Doctores.Servicios;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import com.com.xentinels.amc.client.ActividadDeUsuarios.Doctores.Servicios.ui.doctores.DoctoresFragment;

import com.xentinels.amc.clients.R;

public class Doctores extends AppCompatActivity {
    private String serviceId;
    private String date;
    private DoctoresFragment docs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctores);
        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        if (bundle != null) {
            serviceId = bundle.getString("serviceId");//
            date = bundle.getString("date");
        }
        if (savedInstanceState == null) {
            docs = DoctoresFragment.newInstance(serviceId,date);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, docs)
                    .commitNow();
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        docs.onActivityResult(requestCode, resultCode, intent);
    }
}
