package com.com.xentinels.amc.client.ActividadDeUsuarios.Servicios.ui.servicios;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import androidx.core.content.ContextCompat;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.shivtechs.maplocationpicker.LocationPickerActivity;
import com.shivtechs.maplocationpicker.MapUtility;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.com.xentinels.amc.client.ActividadDeUsuarios.ActivityUsuarios;
import com.com.xentinels.amc.client.ActividadDeUsuarios.Doctores.Servicios.Doctores;
import com.com.xentinels.amc.client.Internet.SolicitudesJson;
import com.com.xentinels.amc.client.Preferences;
import com.xentinels.amc.clients.R;
import com.com.xentinels.amc.client.VolleyRP;


public class AdapterServicios extends RecyclerView.Adapter<AdapterServicios.HolderAmigos> implements DatePickerDialog.OnDateSetListener{

    private List<AtributosServicios> atributosList;
    private Context context;
    private ServiciosFragment f;
    DatePickerDialog.OnDateSetListener dateListener;
    private VolleyRP volley;
    private RequestQueue mRequest;
    private static final int ADDRESS_PICKER_REQUEST = 1020;
    private String txtLatLong;
    private JSONObject mainJson;
    private JSONObject detailsJson;
    private JSONArray infoDetailsJson;
    private int quantity;

    public AdapterServicios(List<AtributosServicios> atributosList, Context context, ServiciosFragment f){
        this.atributosList = atributosList;
        this.context = context;
        this.f=f;
        volley = VolleyRP.getInstance(context);
        mRequest = volley.getRequestQueue();
    }

    @Override
    public HolderAmigos onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_servicios,parent,false);
        return new AdapterServicios.HolderAmigos(v);
    }

    @Override
    public void onBindViewHolder(HolderAmigos holder, final int position) {
        Picasso.get().load(atributosList.get(position).getFoto()).error(R.drawable.ic_account_circle).into(holder.imageView);
        holder.nombre.setText(atributosList.get(position).getNombreCompleto());
        holder.descripcion.setText(atributosList.get(position).getMensaje());
        holder.costo.setText("Costo: $" + atributosList.get(position).getvalueToString());
        if(atributosList.get(position).getMensaje().equals("null")){
            holder.descripcion.setText("Abrir servicio");
        }else{
            if(atributosList.get(position).getType_mensaje().equals("1")){
                holder.descripcion.setTextColor(ContextCompat.getColor(context, R.color.colorBlack));
            }else{
                holder.descripcion.setTextColor(ContextCompat.getColor(context, R.color.colorBlue));
            }

        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                        AlertDialog.Builder myBuilder = new AlertDialog.Builder(context);
                        myBuilder.setMessage("Escoga una fecha para la cita.");
                        myBuilder.setTitle("Confirmación");
                        myBuilder.setCancelable(true);
                        myBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Resources res = context.getResources();
                                DisplayMetrics dm = res.getDisplayMetrics();
                                android.content.res.Configuration conf = res.getConfiguration();
                                conf.locale = new Locale("es");
                                res.updateConfiguration(conf, dm);
                                DatePickerDialog datePicker = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                                    @Override
                                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                        String monthstr;
                                        String daystr;
                                        if (month<10) monthstr = "0"+String.valueOf(month+1); else monthstr = String.valueOf(month+1);
                                        if (day<10) daystr = "0"+String.valueOf(day); else daystr = String.valueOf(day);
                                        String formattedDate = year+"-"+monthstr+"-"+daystr;
                                        Toast.makeText(context,formattedDate,Toast.LENGTH_LONG).show();
                                        String scheduleTime = selectHour(position,formattedDate);
                                        ////////////////////////////////
                                        Intent i = new Intent(context, Doctores.class);
                                        i.putExtra("date",formattedDate);
                                        i.putExtra("serviceId",atributosList.get(position).getId());
                                        //context.startActivity(i);
                                    }
                                }, Calendar.getInstance().get(Calendar.YEAR),Calendar.getInstance().get(Calendar.MONTH),Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

                                Calendar calendar = Calendar.getInstance();
                                datePicker.getDatePicker().setMinDate(calendar.getTimeInMillis());
                                calendar.add(Calendar.DATE, 7);
                                datePicker.getDatePicker().setMaxDate(calendar.getTimeInMillis());
                                datePicker.show();

                            }

                        });
                        AlertDialog dialogDep = myBuilder.create();
                        dialogDep.show();
            }
        });
    }

    private String selectHour(int position, String formattedDate) {
        JsonObjectRequest solicitud = new JsonObjectRequest(Request.Method.GET, SolicitudesJson.URL_AVAILABLE_SCHEDULES,null, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject datos) {

                try {
                    JSONArray schedules = datos.getJSONArray("schedules");////////////------------------
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Horarios Disponibles");
                    HashMap<Integer,String> Schedule = new HashMap<Integer,String>();
                    String start;

                    String[] types = new String[schedules.length()];
                    for (int i = 0; i < schedules.length(); i++) {
                        JSONObject jsonObjectSchedules = schedules.getJSONObject(i);
                        start = jsonObjectSchedules.getString("start");
                        types[i] = start;
                        Schedule.put(i,start);
                    }
                    /////////////////////////////////////////
                    b.setItems(types, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialogint, int which) {

                            dialogint.dismiss();
                            String Hour = Schedule.get(which);

                                    NumberPicker picker = new NumberPicker(context);
                                    picker.setMinValue(1);
                                    picker.setMaxValue(3);

                                    FrameLayout layout = new FrameLayout(context);
                                    layout.addView(picker, new FrameLayout.LayoutParams(
                                            FrameLayout.LayoutParams.WRAP_CONTENT,
                                            FrameLayout.LayoutParams.WRAP_CONTENT,
                                            Gravity.CENTER));

                                    new AlertDialog.Builder(context)
                                            .setView(layout)
                                            .setTitle("Escoja la cantidad de personas para las que se agenda el servicio")
                                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    quantity = picker.getValue();
                                                    mainJson  = new JSONObject();
                                                    infoDetailsJson  = new JSONArray();
                                                    detailsJson  = new JSONObject();
                                                    try {
                                                        detailsJson.put("service", atributosList.get(position).getId());
                                                        detailsJson.put("quantity", quantity);
                                                        infoDetailsJson.put(detailsJson);
                                                        mainJson.put("booking_details", infoDetailsJson);
                                                        mainJson.put("estimated_start_booking", formattedDate+"T"+Hour+":00-05:00");
                                                        mainJson.put("customer", Preferences.obtenerPreferenceString(context,Preferences.PREFERENCE_SESSION_ID_USER));
                                                    } catch ( JSONException e ) {
                                                        e.printStackTrace();
                                                    }
                                                    androidx.appcompat.app.AlertDialog.Builder builder1 = new androidx.appcompat.app.AlertDialog.Builder(context);
                                                    builder1.setTitle("Ubicación");
                                                    builder1.setMessage("Selecciona el lugar donde se realizará la cita");
                                                    builder1.setCancelable(true);
                                                    builder1.setPositiveButton("OK",
                                                            new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialogI, int id) {
                                                                    Intent i = new Intent(context, LocationPickerActivity.class);
                                                                    ((Activity) context).startActivityForResult(i, ADDRESS_PICKER_REQUEST);


                                                                }
                                                            });
                                                    androidx.appcompat.app.AlertDialog alert11 = builder1.create();
                                                    alert11.show();
                                                }
                                            })
                                            .setNegativeButton(android.R.string.cancel, null)
                                            .show();
                        }////////////

                    });

                    b.show();

////////////////////////////////////
                } catch (JSONException e) {
                    Toast.makeText(context,"Ocurrio un error al descomponer el JSON", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String jsonError = "";
                if (networkResponse != null && networkResponse.data != null) {
                    jsonError = new String(networkResponse.data);
                    System.out.println(jsonError);
                    VolleyLog.d("Error:", jsonError);
                    Toast.makeText(context,jsonError, Toast.LENGTH_LONG).show();
                }
            }}){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization","Token "+ Preferences.obtenerPreferenceString(context,Preferences.PREFERENCE_SESSION_TOKEN));
                return headers;
            }
        };
        VolleyRP.addToQueue(solicitud,mRequest,context,volley);

        return "hola";
    }

    @Override
    public int getItemCount() {
        return atributosList.size();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        AlertDialog.Builder myBuilder = new AlertDialog.Builder(context);
        myBuilder.setMessage("Está seguro que desea agendar una cita con los datos proporcionados?");
        myBuilder.setTitle("Confirmación");
        myBuilder.setPositiveButton("Si", new DialogInterface.OnClickListener() {/////////
            @Override
            public void onClick(DialogInterface dialog, int which) {////
                if (requestCode == ADDRESS_PICKER_REQUEST) {
                    try {
                        if (data != null && data.getStringExtra(MapUtility.ADDRESS) != null) {
                            String address = data.getStringExtra(MapUtility.ADDRESS);
                            double selectedLatitude = round(data.getDoubleExtra(MapUtility.LATITUDE, 0.0),6);
                            double selectedLongitude = round(data.getDoubleExtra(MapUtility.LONGITUDE, 0.0),6);
                            txtLatLong = "Lat:"+selectedLatitude+"  Long:"+selectedLongitude;
                            //Toast.makeText(context,txtLatLong,Toast.LENGTH_LONG).show();
                            AgendarCita(selectedLatitude,selectedLongitude,mainJson);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                dialog.cancel();
            }});
        myBuilder.setCancelable(true);
        myBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }

        });
        AlertDialog dialog = myBuilder.create();
        dialog.show();

    }

        public static double round(double value, int places) {
            if (places < 0) throw new IllegalArgumentException();

            long factor = (long) Math.pow(10, places);
            value = value * factor;
            long tmp = Math.round(value);
            return (double) tmp / factor;
        }
    private void AgendarCita(double selectedLatitude, double selectedLongitude, JSONObject mainJson) {
        try{
            mainJson.put("latitude",selectedLatitude);
            mainJson.put("longitude",selectedLongitude);
            JsonObjectRequest solicitud = new JsonObjectRequest(Request.Method.POST, SolicitudesJson.URL_NEW_BOOKING,mainJson, new Response.Listener<JSONObject>(){
                @Override
                public void onResponse(JSONObject datos) {

                    androidx.appcompat.app.AlertDialog.Builder builder1 = new androidx.appcompat.app.AlertDialog.Builder(context);
                    builder1.setTitle("Correcto");
                    builder1.setMessage("La cita se agendó correctamente, espere que sea aceptada para pagarla");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent i = new Intent(context, ActivityUsuarios.class);
                                    context.startActivity(i);
                                    ((Activity)context).finish();
                                    dialog.cancel();
                                }
                            });
                    androidx.appcompat.app.AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
            },new Response.ErrorListener(){
                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse networkResponse = error.networkResponse;
                    String jsonError = "";
                    if (networkResponse != null && networkResponse.data != null) {
                        jsonError = new String(networkResponse.data);
                        System.out.println(jsonError);
                        VolleyLog.d("Error:", jsonError);
                        Toast.makeText(context,jsonError, Toast.LENGTH_LONG).show();
                    }
                }}){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization","Token "+ Preferences.obtenerPreferenceString(context,Preferences.PREFERENCE_SESSION_TOKEN));
                    return headers;
                }
            };
            VolleyRP.addToQueue(solicitud,mRequest,context,volley);

        } catch ( JSONException e ) {
            e.printStackTrace();
        }


    }

    static class HolderAmigos extends RecyclerView.ViewHolder{

        CardView cardView;
        ImageView imageView;
        TextView nombre;
        TextView descripcion;
        TextView costo;

        public HolderAmigos(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cardViewServicios);
            imageView = itemView.findViewById(R.id.fotoDePerfilServicio);
            nombre = itemView.findViewById(R.id.nombreDependiente);
            descripcion = itemView.findViewById(R.id.generoDependiente);
            costo = itemView.findViewById(R.id.fechaNacDependiente);

        }
    }

}
