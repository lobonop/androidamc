package com.com.xentinels.amc.client.ActividadDeUsuarios.Servicios;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.com.xentinels.amc.client.ActividadDeUsuarios.Servicios.ui.servicios.ServiciosFragment;

import com.xentinels.amc.clients.R;

public class Servicios extends AppCompatActivity {
    private String CATEGORY_SERVICE;
    Context mContext;
    private ServiciosFragment serviciosFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicios);

        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        if (bundle != null) {
            CATEGORY_SERVICE = bundle.getString("CATEGORY_SERVICE");//
        }
        if (savedInstanceState == null) {
            serviciosFragment = ServiciosFragment.newInstance(CATEGORY_SERVICE);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, serviciosFragment)
                    .commitNow();
        }
    }
    @SuppressLint("MissingSuperCall")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        serviciosFragment.onActivityResult(requestCode, resultCode, intent);
    }
}
