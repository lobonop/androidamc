package com.com.xentinels.amc.client.ActividadDeUsuarios;

import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.com.xentinels.amc.client.ActividadDeUsuarios.Citas.FragmentCitas;

import java.util.HashMap;

import com.com.xentinels.amc.client.ActividadDeUsuarios.Categorias.FragmentCategorias;


public class AdapterUsuarios extends FragmentPagerAdapter {

    HashMap<Integer,String> mFragmentTags = new HashMap<Integer,String>();
    FragmentManager mFragmentManager;
    public AdapterUsuarios(FragmentManager fm) {
        super(fm);
        mFragmentManager = fm;
    }
//    String tabTitles[] = new String[] { "CATEGORÍAS", "CITAS", "CHAT"};
    String tabTitles[] = new String[] { "CATEGORÍAS", "CITAS"};

    public Fragment[] fragments = new Fragment[tabTitles.length];
    @Override
    public Fragment getItem(int position) {
        if(position==0) return new FragmentCategorias();
        else if(position==1) return new FragmentCitas();
        //else if(position==2) return new FragmentCategorias();
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment createdFragment = (Fragment) super.instantiateItem(container, position);
        fragments[position]  = createdFragment;
        String tag = createdFragment.getTag();
        mFragmentTags.put(position, tag);
        return createdFragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(position==0) return "CATEGORÍAS ";
        else if(position==1)return "CITAS";
        //else if(position==2)return "CHAT";
        return null;
    }

    public Fragment getFragment(int position) {
        String tag = mFragmentTags.get(position);
        if (tag == null)
            return null;
        return mFragmentManager.findFragmentByTag(tag);
    }
}
