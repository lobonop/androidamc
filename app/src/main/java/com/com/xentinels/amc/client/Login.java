package com.com.xentinels.amc.client;

import android.app.NotificationManager;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import com.andreabaccega.widget.FormEditText;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.com.xentinels.amc.client.ActividadDeUsuarios.ActivityUsuarios;
import com.com.xentinels.amc.client.ActividadDeUsuarios.Cards.Alert;
import com.com.xentinels.amc.client.Internet.SolicitudesJson;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.paymentez.android.Paymentez;

import org.json.JSONException;
import org.json.JSONObject;

import com.xentinels.amc.clients.R;
import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {

    private FormEditText eTusuario;
    private FormEditText eTcontraseña;
    private Button bTingresar;
    private Button registro;
    private String token;
    private RadioButton RBsesion;

    private VolleyRP volley;
    private RequestQueue mRequest;

    private String USER = "";
    private String PASSWORD = "";
    private SolicitudesJson urls;
    private boolean isActivateRadioButton;
    private static final String TAG = "RegIntentService";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        volley = VolleyRP.getInstance(this);
        mRequest = volley.getRequestQueue();
        /**
         * Init library
         *
         * @param test_mode false to use production environment
         * @param paymentez_client_app_code provided by Paymentez.
         * @param paymentez_client_app_key provided by Paymentez.
         */
        getPaymentezCredentials();
        if(Preferences.obtenerPreferenceBoolean(Login.this,Preferences.PREFERENCE_SESSION_KEEP_LOGGED_IN)) {
        login();
        }

        eTusuario = findViewById(R.id.eTusuario);
        eTcontraseña = findViewById(R.id.eTcontraseña);

        bTingresar = findViewById(R.id.bTingresar);
        registro = findViewById(R.id.registrar);

        RBsesion = findViewById(R.id.RBSecion);

        isActivateRadioButton = RBsesion.isChecked(); //DESACTIVADO

        RBsesion.setOnClickListener(new View.OnClickListener() {
            //ACTIVADO
            @Override
            public void onClick(View v) {
                if(isActivateRadioButton){
                    RBsesion.setChecked(false);
                }
                isActivateRadioButton = RBsesion.isChecked();
            }
        });

        bTingresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    VerificarLogin(eTusuario.getText().toString().toLowerCase(),eTcontraseña.getText().toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Login.this,Registro.class);
                startActivity(i);
            }
        });
    }

    private void getPaymentezCredentials() {
        JsonObjectRequest solicitud = new JsonObjectRequest(Request.Method.GET,SolicitudesJson.URL_PUBLIC_CREDENTIALS,null, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject datos) {
                try {
                    Boolean enviroment = true;
                    if(datos.getString("environment").equals("dev")){
                        enviroment=true;
                    }else{
                        enviroment=false;
                    }
                    Preferences.savePreferenceBoolean(Login.this,enviroment,Preferences.PAYMENTEZ_IS_TEST_MODE);
                    Preferences.savePreferenceString(Login.this,datos.getString("client_code"),Preferences.PAYMENTEZ_CLIENT_APP_CODE);
                    Preferences.savePreferenceString(Login.this,datos.getString("client_key"),Preferences.PAYMENTEZ_CLIENT_APP_KEY);
                    Paymentez.setEnvironment(Preferences.obtenerPreferenceBoolean(Login.this,Preferences.PAYMENTEZ_IS_TEST_MODE), Preferences.obtenerPreferenceString(Login.this,Preferences.PAYMENTEZ_CLIENT_APP_CODE), Preferences.obtenerPreferenceString(Login.this,Preferences.PAYMENTEZ_CLIENT_APP_KEY));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String jsonError="";
                if (networkResponse != null && networkResponse.data != null) {
                    jsonError = new String(networkResponse.data);
                    VolleyLog.d("Error:", jsonError);
                    /*try {
                        Toast.makeText(Login.this,new JSONObject(jsonError).getString(Preferences.PREFERENCE_ERROR_FIELD),Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/
                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        VolleyRP.addToQueue(solicitud,mRequest,this,volley);


    }

    public void VerificarLogin(String user, String password) throws JSONException {
        if(validate()){
        USER = user;
        PASSWORD = password;
        Map<String, String> params = new HashMap();
        params.put("username", USER);
        params.put("password", PASSWORD);
        JSONObject loginParams = new JSONObject(params);
        SolicitudJSON(SolicitudesJson.URL_LOGIN,loginParams);
    }
    }
    public void login(){

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            Alert.show(Login.this,"Login","No se  puede iniciar sesión verifique su conexión a internet e intenta de nuevo");
                            return;
                        }
                        token = task.getResult().getToken();
                        //Log.d(TAG, token);
                        registerPushNotification();
                        RunLogedInUser();
                    }
    });

    }

    public void SolicitudJSON(String URL,JSONObject loginParams){
        JsonObjectRequest solicitud = new JsonObjectRequest(Request.Method.POST,URL,loginParams, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject datos) {
                try {
                    String token = datos.getString("token");
                    //Toast.makeText(Login.this,"el token es: "+token,Toast.LENGTH_LONG).show();
                    Preferences.savePreferenceString(Login.this,token,Preferences.PREFERENCE_SESSION_TOKEN);
                    Preferences.savePreferenceBoolean(Login.this,isActivateRadioButton,Preferences.PREFERENCE_SESSION_KEEP_LOGGED_IN);
                    Preferences.savePreferenceBoolean(Login.this,datos.getBoolean("is_verified"),Preferences.IS_VERIFIED);
                    Preferences.savePreferenceString(Login.this,datos.getString("user_id"),Preferences.PREFERENCE_SESSION_ID_USER);
                    Preferences.savePreferenceString(Login.this,datos.getString("email"),Preferences.PREFERENCE_SESSION_EMAIL_USER);
                    Preferences.savePreferenceString(Login.this,"+593" + datos.getString("phone"),Preferences.PREFERENCE_USER_PHONE);
                    login();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String jsonError="";
                if (networkResponse != null && networkResponse.data != null) {
                    jsonError = new String(networkResponse.data);
                    VolleyLog.d("Error:", jsonError);
                    /*try {
                        Toast.makeText(Login.this,new JSONObject(jsonError).getString(Preferences.PREFERENCE_ERROR_FIELD),Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/
                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        VolleyRP.addToQueue(solicitud,mRequest,this,volley);
    }
    private boolean validate() {
        FormEditText[] allFields={eTusuario, eTcontraseña};
        boolean allValid = true;
        for (FormEditText field : allFields) {
            allValid = field.testValidity() && allValid;
        }
        return allValid;
    }

    public void RunLogedInUser(){
        NotificationManager notificationmanager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        Boolean isVerified = Preferences.obtenerPreferenceBoolean(Login.this,Preferences.IS_VERIFIED);
        if(isVerified) {
            Intent i = new Intent(Login.this, ActivityUsuarios.class);
            startActivity(i);
            finish();
        }else{
            Intent intent = new Intent(Login.this, VerifyPhoneActivity.class);
            intent.putExtra("phonenumber", Preferences.obtenerPreferenceString(this,Preferences.PREFERENCE_USER_PHONE));
            startActivity(intent);
        }
    }

    private void registerPushNotification() {
        String deviceAppUID = FirebaseInstanceId.getInstance().getId();
        HashMap<String, String> body = new HashMap<String, String>();
        body.put("device_uid",deviceAppUID);
        body.put("device_token",token);
        body.put("type","android");
        Log.d("PUSH PARAMETERS>","device_uid " + deviceAppUID);
        Log.d("PUSH PARAMETERS>","device_token "+ token);
        Log.d("PUSH PARAMETERS>","type "+"android");

        JsonObjectRequest solicitud = new JsonObjectRequest(Request.Method.POST, SolicitudesJson.URL_REGISTER_TO_PUSH_NOTIFICATIONS,new JSONObject(body), new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject datos) {

                try {
                    String results = datos.getString("status");
                } catch (JSONException e) {
                    Toast.makeText(Login.this,"Ocurrió un error interno, intente de nuevo", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String jsonError = "";
                if (networkResponse != null && networkResponse.data != null) {
                    jsonError = new String(networkResponse.data);
                    System.out.println(jsonError);
                    VolleyLog.d("Error:", jsonError);
                    Toast.makeText(Login.this,jsonError, Toast.LENGTH_LONG).show();
                }
            }}){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization","Token "+ Preferences.obtenerPreferenceString(Login.this,Preferences.PREFERENCE_SESSION_TOKEN));
                return headers;
            }
        };
        VolleyRP.addToQueue(solicitud,mRequest,Login.this,volley);

    }

}
