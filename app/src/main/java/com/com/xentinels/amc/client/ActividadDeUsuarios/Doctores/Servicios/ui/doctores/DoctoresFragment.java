package com.com.xentinels.amc.client.ActividadDeUsuarios.Doctores.Servicios.ui.doctores;

import androidx.lifecycle.ViewModelProviders;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.shivtechs.maplocationpicker.MapUtility;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.com.xentinels.amc.client.Internet.SolicitudesJson;
import com.com.xentinels.amc.client.Preferences;
import com.xentinels.amc.clients.R;
import com.com.xentinels.amc.client.VolleyRP;

public class DoctoresFragment extends Fragment {

    private DoctoresViewModel mViewModel;
    Context thiscontext;
    private VolleyRP volley;
    private RequestQueue mRequest;
    private RecyclerView rv;
    private List<AtributosDoctores> atributosList;
    private AdapterDoctores adapter;
    private LinearLayout layoutVacio;
    private EventBus bus = EventBus.getDefault();
    protected static String serviceId;
    private static String date;
    private ProgressDialog pd;

    public static DoctoresFragment newInstance(String CATEGORY_SERVICE,String DATE) {
        serviceId = CATEGORY_SERVICE;
        date = DATE;
        return new DoctoresFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        thiscontext = container.getContext();
        View v = inflater.inflate(R.layout.fragment_doctores, container, false);
        pd = new ProgressDialog(thiscontext);
        pd.setMessage("");
        pd.show();
        Toolbar toolbar = v.findViewById(R.id.toolbarDoctores);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
                getActivity().finish();
            }
        });

        volley = VolleyRP.getInstance(getActivity());
        mRequest = volley.getRequestQueue();
        atributosList = new ArrayList<AtributosDoctores>();

        rv = v.findViewById(R.id.doctoresRecyclerView);
        layoutVacio = v.findViewById(R.id.layoutVacioDoctores);
        LinearLayoutManager lm = new LinearLayoutManager(getContext());
        rv.setLayoutManager(lm);
        adapter = new AdapterDoctores(atributosList,getContext(),this);
        MapUtility.apiKey = getResources().getString(R.string.your_api_key);
        rv.setAdapter(adapter);
        cargarDoctores();
        verificarSiExistenServicios();
        return v;

    }
    public void verificarSiExistenServicios(){
        if(atributosList.isEmpty()){
            layoutVacio.setVisibility(View.VISIBLE);
            rv.setVisibility(View.GONE);
        }else{
            layoutVacio.setVisibility(View.GONE);
            rv.setVisibility(View.VISIBLE);
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        adapter.onActivityResult(requestCode, resultCode, data);
    }

    private void actualizarTarjetas(){
        adapter.notifyDataSetChanged();
        verificarSiExistenServicios();
    }

    public void agregarDoctor(String id, String name, String lastname, String email, String phone, String gender, String born_date, String imageURL, String identification, String college_name, String career_name, HashMap<String, String> schedules){
        AtributosDoctores serviciosAtributos = new AtributosDoctores(id, name, lastname, email, phone, gender, born_date, imageURL,identification,college_name,career_name,schedules);
        atributosList.add(serviciosAtributos);
        adapter.notifyDataSetChanged();
        verificarSiExistenServicios();
    }

    public void agregarServicio(AtributosDoctores a){
        atributosList.add(0,a);
        adapter.notifyDataSetChanged();
        verificarSiExistenServicios();
    }

    public void cargarDoctores(){

        JsonObjectRequest solicitud = new JsonObjectRequest(Request.Method.GET, SolicitudesJson.URL_GET_DOCTORS+"?service__code="+serviceId+"&date="+date,null, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject datos) {
                pd.dismiss();
                try {
                    String results = datos.getString("results");
                    JSONArray resultsArray = new JSONArray(results);
                    for(int i =0;i<resultsArray.length();i++) {
                        JSONObject jsonObject = new JSONObject(resultsArray.getString(i));
                        JSONObject provider = jsonObject.getJSONObject("provider");
                        String id = provider.getString("id");
                        String name = provider.getString("first_name");
                        String lastname = provider.getString("last_name");
                        String email = provider.getString("email");
                        String phone = provider.getString("phone");
                        JSONObject profile = provider.getJSONObject("profile");
                        String gender = profile.getString("gender");
                        String born_date = profile.getString("born_date");
                        String imageURL = profile.getString("avatar");
                        JSONObject profileprovider = provider.getJSONObject("profileprovider");
                        String identification = profileprovider.getString("identification");
                        String college_name = profileprovider.getString("college_name");
                        String career_name = profileprovider.getString("career_name");
                        JSONArray schedulesArray = provider.getJSONArray("schedules");
                        String Start;
                        String End;
                        HashMap<String,String> Schedule = new HashMap<String,String>();

                        for (int j = 0; j < schedulesArray.length(); j++) {
                            JSONObject jsonObjectSchedules = schedulesArray.getJSONObject(j);
                            Start = jsonObjectSchedules.getString("start");
                            End = jsonObjectSchedules.getString("end");
                            Schedule.put(Start,End);
                        }
                        agregarDoctor(id, name, lastname, email, phone, gender, born_date, imageURL,identification,college_name,career_name,Schedule);
                    }
                } catch (JSONException e) {
                    Toast.makeText(getContext(),"Ocurrio un error al descomponer el JSON",Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String jsonError = "";
                if (networkResponse != null && networkResponse.data != null) {
                    jsonError = new String(networkResponse.data);
                    System.out.println(jsonError);
                    VolleyLog.d("Error:", jsonError);
                    Toast.makeText(thiscontext,jsonError, Toast.LENGTH_LONG).show();
                }
            }}){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization","Token "+ Preferences.obtenerPreferenceString(thiscontext,Preferences.PREFERENCE_SESSION_TOKEN));
                return headers;
            }
        };
        VolleyRP.addToQueue(solicitud,mRequest,thiscontext,volley);
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void ejecutarLLamada(AtributosDoctores a){
        agregarServicio(a);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void limpiarLista(){
        atributosList.clear();
        actualizarTarjetas();
    }

    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(DoctoresViewModel.class);
        // TODO: Use the ViewModel
    }

}
