package com.com.xentinels.amc.client.ActividadDeUsuarios.Categorias;

import android.content.Context;
import android.content.Intent;
import androidx.core.content.ContextCompat;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import com.com.xentinels.amc.client.ActividadDeUsuarios.Servicios.Servicios;
import com.xentinels.amc.clients.R;



public class AdapterCategorias extends RecyclerView.Adapter<AdapterCategorias.HolderAmigos> {

    private List<AtributosCategorias> atributosList;
    private Context context;
    private FragmentCategorias f;


    public AdapterCategorias(List<AtributosCategorias> atributosList, Context context, FragmentCategorias f){
        this.atributosList = atributosList;
        this.context = context;
        this.f=f;
    }

    @Override
    public HolderAmigos onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_categorias,parent,false);
        return new AdapterCategorias.HolderAmigos(v);
    }

    @Override
    public void onBindViewHolder(HolderAmigos holder, final int position) {
        Picasso.get().load(atributosList.get(position).getFoto()).error(R.drawable.ic_account_circle).into(holder.imageView);
        holder.nombre.setText(atributosList.get(position).getNombreCompleto());
        holder.mensaje.setText(atributosList.get(position).getMensaje());
        holder.hora.setText(atributosList.get(position).getId());

        if(atributosList.get(position).getMensaje().equals("null")){
            holder.hora.setVisibility(View.GONE);
            holder.mensaje.setText("Abrir categoría");
        }else{
            holder.hora.setVisibility(View.VISIBLE);

            if(atributosList.get(position).getType_mensaje().equals("1")){
                holder.mensaje.setTextColor(ContextCompat.getColor(context, R.color.colorBlack));
            }else{
                holder.mensaje.setTextColor(ContextCompat.getColor(context, R.color.colorBlue));
            }

        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, Servicios.class);
                i.putExtra("CATEGORY_SERVICE",atributosList.get(position).getId());
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return atributosList.size();
    }

    static class HolderAmigos extends RecyclerView.ViewHolder{

        CardView cardView;
        ImageView imageView;
        TextView nombre;
        TextView mensaje;
        TextView hora;

        public HolderAmigos(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cardViewAmigos);
            imageView = itemView.findViewById(R.id.fotoDePerfilAmigos);
            nombre = itemView.findViewById(R.id.nombreUsuarioAmigo);
            mensaje = itemView.findViewById(R.id.mensajeAmigos);
            hora = itemView.findViewById(R.id.horaAmigos);
        }
    }

}
