package com.com.xentinels.amc.client.ActividadDeUsuarios.Categorias;

import com.com.xentinels.amc.client.ActividadDeUsuarios.ClasesComunicacion.Usuario;

/**
 * Created by user on 8/05/2017.
 */

public class AtributosCategorias extends Usuario {

    private String type_mensaje;

    public AtributosCategorias() {
    }

    public AtributosCategorias(String id, String nombreCompleto, String descripcion, String hora, String urlFotoServicio, String type_mensaje) {
        super(id, nombreCompleto, descripcion, urlFotoServicio);
        this.type_mensaje = type_mensaje;
    }

    public String getType_mensaje() {
        return type_mensaje;
    }

    public void setType_mensaje(String type_mensaje) {
        this.type_mensaje = type_mensaje;
    }
}
