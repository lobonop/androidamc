package com.com.xentinels.amc.client.ActividadDeUsuarios.proveedores.ui.proveedor;

import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xentinels.amc.clients.R;

public class ProveedorFragment extends Fragment {

    private ProveedorViewModel mViewModel;

    public static ProveedorFragment newInstance() {
        return new ProveedorFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_proveedor, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(ProveedorViewModel.class);
        // TODO: Use the ViewModel
    }

}
