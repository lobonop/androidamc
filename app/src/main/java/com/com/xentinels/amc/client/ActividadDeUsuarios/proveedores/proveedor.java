package com.com.xentinels.amc.client.ActividadDeUsuarios.proveedores;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.com.xentinels.amc.client.ActividadDeUsuarios.proveedores.ui.proveedor.ProveedorFragment;

import com.xentinels.amc.clients.R;

public class proveedor extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proveedor);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, ProveedorFragment.newInstance())
                    .commitNow();
        }
    }
}
