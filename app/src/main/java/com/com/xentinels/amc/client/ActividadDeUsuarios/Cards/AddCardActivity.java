package com.com.xentinels.amc.client.ActividadDeUsuarios.Cards;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.paymentez.android.Paymentez;
import com.paymentez.android.model.Card;
import com.paymentez.android.rest.TokenCallback;
import com.paymentez.android.rest.model.PaymentezError;
import com.paymentez.android.view.CardMultilineWidget;
import com.xentinels.amc.clients.R;
import com.com.xentinels.amc.client.Preferences;


public class AddCardActivity extends AppCompatActivity {

    Button buttonNext;
    CardMultilineWidget cardWidget;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card);
        mContext = this;
        Toolbar toolbar = findViewById(R.id.toolbarAddCard);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        final String uid = Preferences.obtenerPreferenceString(this, Preferences.PREFERENCE_SESSION_ID_USER);
        final String email = Preferences.obtenerPreferenceString(this, Preferences.PREFERENCE_SESSION_EMAIL_USER);

        cardWidget = (CardMultilineWidget) findViewById(R.id.card_multiline_widget);
        buttonNext = (Button) findViewById(R.id.buttonAddCard);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                buttonNext.setEnabled(false);

                Card cardToSave = cardWidget.getCard();
                if (cardToSave == null) {
                    buttonNext.setEnabled(true);
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                    builder1.setTitle("Error");
                    builder1.setMessage("Invalid Card Data");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                    return;
                }else{
                    final ProgressDialog pd = new ProgressDialog(AddCardActivity.this);
                    pd.setMessage("");
                    pd.show();

                    Paymentez.addCard(mContext, uid, email, cardToSave, new TokenCallback() {

                        public void onSuccess(Card card) {
                            buttonNext.setEnabled(true);
                            pd.dismiss();
                            if(card != null){
                                if(card.getStatus().equals("valid")){
                                    Alert.show(mContext,
                                            "Card Successfully Added",
                                                    "transaction_reference: " + card.getTransactionReference());

                                } else if (card.getStatus().equals("review")) {
                                    Alert.show(mContext,
                                            "Card Under Review",
                                            "status: " + card.getStatus() + "\n" +
                                                    "transaction_reference: " + card.getTransactionReference());

                                } else {
                                    Alert.show(mContext,
                                            "Error",
                                            "status: " + card.getStatus() + "\n" +
                                                    "message: " + card.getMessage());
                                }
                                onBackPressed();
                                finish();

                            }

                            //TODO: Create charge or Save Token to your backend
                        }

                        public void onError(PaymentezError error) {
                            buttonNext.setEnabled(true);
                            pd.dismiss();
                            Alert.show(mContext,
                                    "Error",
                                    "Type: " + error.getType() + "\n" +
                                            "Help: " + error.getHelp() + "\n" +
                                            "Description: " + error.getDescription());

                            //TODO: Handle error
                        }

                    });

                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
