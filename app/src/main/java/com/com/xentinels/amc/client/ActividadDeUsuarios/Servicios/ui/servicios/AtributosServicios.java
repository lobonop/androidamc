package com.com.xentinels.amc.client.ActividadDeUsuarios.Servicios.ui.servicios;

import com.com.xentinels.amc.client.ActividadDeUsuarios.ClasesComunicacion.Usuario;

/**
 * Created by user on 8/05/2017.
 */

public class AtributosServicios extends Usuario{

    private String type_mensaje;
    private Double value;

    public AtributosServicios() {
    }

    public AtributosServicios(String id, String nombreCompleto, String descripcion, String hora, String urlFotoServicio, String type_mensaje, Double value) {
        super(id, nombreCompleto, descripcion, urlFotoServicio);
        this.type_mensaje = type_mensaje;
        this.value = value;
    }

    public String getType_mensaje() {
        return type_mensaje;
    }

    public void setType_mensaje(String type_mensaje) {
        this.type_mensaje = type_mensaje;
    }

    public String getvalueToString() {
        return String.valueOf(value);
    }

    public void setValue(Double value) {
        this.value = value;
    }
}
