package com.com.xentinels.amc.client.ActividadDeUsuarios.Citas;

import android.content.Context;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.com.xentinels.amc.client.ActividadDeUsuarios.Citas.Details.BookingDetail;
import com.squareup.picasso.Picasso;

import java.util.List;

import com.xentinels.amc.clients.R;



public class CitasAdapter extends RecyclerView.Adapter<CitasAdapter.citasHolder> {

    private List<Citas> listCitas;
    private Context context;
    private FragmentCitas f;

    public CitasAdapter(List<Citas> listCitas, Context context, FragmentCitas f){
        this.listCitas = listCitas;
        this.context = context;
        this.f = f;
    }

    @Override
    public citasHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_citas,parent,false);
        return new citasHolder(v);
    }

    @Override
    public void onBindViewHolder(citasHolder holder, final int position) {

        Picasso.get().load(R.drawable.citamedica).error(R.drawable.ic_account_circle).into(holder.imgCita);
        holder.nombreServicioCita.setText(listCitas.get(position).getNombreServicio());
        boolean pagar = false;
        boolean tracking = false;
        holder.total.setText("Total: $" + listCitas.get(position).getTotal());
        holder.hinicio.setText("Hora inicio: " + listCitas.get(position).getHoraInicio());
        holder.hfin.setText("Hora fin: " + listCitas.get(position).getHoraFin());
        holder.code = listCitas.get(position).getCode();
        String status = listCitas.get(position).getEstado();
        if (status.equals("Pendiente") || status.equals("Creado")) {
            holder.status.setTextColor(Color.YELLOW);
        } else if (status.equals("Rechazado") || status.equals("Cancelado")) {
            holder.status.setTextColor(Color.RED);
        } else if (status.equals("Aceptado")) {
            holder.status.setTextColor(Color.GREEN);
            status += ", click para pagar";
            pagar = true;
        } else if (status.equals("Pagado")) {
            holder.status.setTextColor(Color.GREEN);
            tracking = true;
        } else if (status.equals("Finalizado") || status.equals("Caducado")) {
            holder.status.setTextColor(Color.BLUE);
        }
        holder.status.setText("Estado: " + status);

        boolean finalPagar = pagar;
        boolean finalTracking = tracking;
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, BookingDetail.class);
                i.putExtra("pagar", finalPagar);
                i.putExtra("tracking", finalTracking);
                i.putExtra("code", listCitas.get(position).getCode());
                context.startActivity(i);
            }
        });
    }



    @Override
    public int getItemCount() {
        return listCitas.size();
    }

    static class citasHolder extends RecyclerView.ViewHolder{

        CardView cardView;
        ImageView imgCita;
        TextView nombreServicioCita;
        TextView status;
        TextView total;
        TextView hinicio;
        TextView hfin;
        String code;

        public citasHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cardViewCitas);
            imgCita = itemView.findViewById(R.id.fotoCitas);
            nombreServicioCita = itemView.findViewById(R.id.nombreServicioCita);
            status = itemView.findViewById(R.id.estadoCita);
            total = itemView.findViewById(R.id.totalCita);
            hinicio = itemView.findViewById(R.id.horaInicio);
            hfin = itemView.findViewById(R.id.horaFin);

        }
    }



}

