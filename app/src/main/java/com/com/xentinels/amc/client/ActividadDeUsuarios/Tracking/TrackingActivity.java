package com.com.xentinels.amc.client.ActividadDeUsuarios.Tracking;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.LinearInterpolator;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.com.xentinels.amc.client.ActividadDeUsuarios.Cards.Alert;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import com.com.xentinels.amc.client.Internet.SolicitudesJson;
import com.com.xentinels.amc.client.Preferences;
import com.xentinels.amc.clients.R;
import com.com.xentinels.amc.client.VolleyRP;

public class TrackingActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private String code;
    private ProgressDialog pd;
    private Location BookingLatLong;
    private Marker mMarker;
    private VolleyRP volley;
    private RequestQueue mRequest;
    private String Name;
    private final static int INTERVAL = 1000 * 5; //5 seconds
    private Handler handler = new Handler();
    private Runnable myRunnable;
    private int delay = 5*1000;
    private Location targetLocations;
    private Double oldLat;
    private Double oldLon;
    private Marker mMarkerPrincipal;
    LatLngBounds.Builder builder;
    CameraUpdate cu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        volley = VolleyRP.getInstance(TrackingActivity.this);
        mRequest = volley.getRequestQueue();
        //final ProgressDialog pd = new ProgressDialog(TrackingActivity.this);

        if (bundle != null) {
            code = bundle.getString("bookingCode");
            BookingLatLong = new Location("");
            BookingLatLong.setLatitude(bundle.getDouble("lat"));
            BookingLatLong.setLongitude(bundle.getDouble("long"));
        }
        //pd.setMessage("");
        //pd.show();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        mMap.getUiSettings().setScrollGesturesEnabled(false);
        LatLng BookingLatLongs = new LatLng(BookingLatLong.getLatitude() ,BookingLatLong.getLongitude());
        mMarkerPrincipal = mMap.addMarker(new MarkerOptions()
                .position(BookingLatLongs)
                .title("paciente")
                .snippet(Preferences.obtenerPreferenceString(TrackingActivity.this,Preferences.PREFERENCE_SESSION_EMAIL_USER)));
        mMarkerPrincipal.showInfoWindow();

        mMap.animateCamera( CameraUpdateFactory.newLatLngZoom( BookingLatLongs, 16 ), 1000, null);
        mMap.moveCamera(CameraUpdateFactory.newLatLng( BookingLatLongs ));
        //animateMarkerNew(BookingLatLong,mMarkerPrincipal,Preferences.obtenerPreferenceString(TrackingActivity.this,Preferences.PREFERENCE_SESSION_EMAIL_USER),"Paciente");
        //getLastProviderUbication();

        /*myRunnable = new Runnable() {
            public void run() {
                getLastProviderUbication();
                handler.postDelayed(myRunnable, delay);
            }
        };*/
        oldLat = -0.22985;
        oldLon = -78.52495;
        BookingLatLongs = new LatLng(-0.22985,-78.52495);
        mMarker = mMap.addMarker(new MarkerOptions()
                .position(BookingLatLongs)
                .title("Doctor")
                .snippet(Preferences.obtenerPreferenceString(TrackingActivity.this,Preferences.PREFERENCE_SESSION_EMAIL_USER))
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.icon_doctor)));
        mMarker.showInfoWindow();
        mMap.animateCamera( CameraUpdateFactory.newLatLngZoom( BookingLatLongs, 15.5f ), 1000, null);
        mMap.moveCamera(CameraUpdateFactory.newLatLng( BookingLatLongs ));
        zoomToFitMarkers(mMarker,mMarkerPrincipal);

        handler.postDelayed( new CustomRunnable(mMarker,TrackingActivity.this,handler) , delay);

    }



void getLastProviderUbication(Marker mMarker){
    String latlongget = "";

    JsonObjectRequest solicitud = new JsonObjectRequest(Request.Method.GET, SolicitudesJson.URL_GET_PROVIDER_LOCATION+code,null, new Response.Listener<JSONObject>(){
        @Override
        public void onResponse(JSONObject datos) {
            try {
                if(datos.getString("status").equals("IN_PROGRESS")){

                    Alert.show(TrackingActivity.this,"INFORMACIÓN","El médico a llegado, sal a recibirlo");
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(TrackingActivity.this);
                    builder1.setTitle("INFORMACIÓN");
                    builder1.setMessage("El médico a llegado, sal a recibirlo");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    finish();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();


                }
                if(distanceBetween(oldLat,oldLon,datos.getDouble("latitude"),datos.getDouble("longitude"))>Double.valueOf(1)){
                    //Alert.show(TrackingActivity.this,"cambio de posicion","asd");
                    JSONObject provider = datos.getJSONObject("provider");
                    Name = provider.getString("first_name") + provider.getString("last_name");

                    Location newloc = new Location("");
                    newloc.setLatitude(datos.getDouble("latitude"));
                    newloc.setLongitude(datos.getDouble("longitude"));

                    oldLat = datos.getDouble("latitude");
                    oldLon = datos.getDouble("longitude");

                    animateMarkerNew(newloc,mMarker,Name,"Médico");

                }

            } catch (JSONException e) {
                Toast.makeText(TrackingActivity.this,"Ocurrió un error interno, intente de nuevo",Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    },new Response.ErrorListener(){
        @Override
        public void onErrorResponse(VolleyError error) {
            NetworkResponse networkResponse = error.networkResponse;
            String jsonError = "";
            if (networkResponse != null && networkResponse.data != null) {
                jsonError = new String(networkResponse.data);
                System.out.println(jsonError);
                VolleyLog.d("Error:", jsonError);
                Toast.makeText(TrackingActivity.this,jsonError, Toast.LENGTH_LONG).show();
            }
        }}){
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            HashMap<String, String> headers = new HashMap<String, String>();
            headers.put("Content-Type", "application/json");
            headers.put("Authorization","Token "+ Preferences.obtenerPreferenceString(TrackingActivity.this,Preferences.PREFERENCE_SESSION_TOKEN));
            return headers;
        }
    };
    VolleyRP.addToQueue(solicitud,mRequest,TrackingActivity.this,volley);
    }

    public void zoomToFitMarkers(Marker... markers) {
        LatLngBounds.Builder b = new LatLngBounds.Builder();
        for (Marker m : markers) {
            b.include(m.getPosition());
        }
        LatLngBounds bounds = b.build();
        CameraUpdate cu = CameraUpdateFactory
                .newLatLngBounds(bounds,150);
        mMap.animateCamera(cu);
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("hasBackPressed",true);
        setResult(TrackingActivity.RESULT_OK,returnIntent);
        finish();
    }


    private double distanceBetween(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = dist * 180.0 / Math.PI;
        dist = dist * 60 * 1.1515*1000;
        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }


    private void animateMarkerNew(final Location destination, final Marker marker,final String name, final String title) {


        if (marker != null) {

            final LatLng startPosition = marker.getPosition();
            final LatLng endPosition = new LatLng(destination.getLatitude(), destination.getLongitude());

            final float startRotation = marker.getRotation();
            final LatLngInterpolatorNew latLngInterpolator = new LatLngInterpolatorNew.LinearFixed();

            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(3000); // duration 3 second
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                        marker.setPosition(newPosition);
                        marker.setTitle(title);
                        marker.setSnippet(name);
                        marker.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.icon_doctor));
                        marker.showInfoWindow();

                        LatLngBounds.Builder b = new LatLngBounds.Builder();
                        //for (Marker m : markers) {
                        b.include(marker.getPosition());
                        b.include(mMarkerPrincipal.getPosition());
                        //}
                        LatLngBounds bounds = b.build();
                        CameraUpdate cu = CameraUpdateFactory
                                .newLatLngBounds(bounds,100);

                       /* mMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                                .target(newPosition)
                                .build()));*/
                        mMap.moveCamera(cu);
                        marker.setRotation(getBearing(startPosition, new LatLng(destination.getLatitude(), destination.getLongitude())));
                    } catch (Exception ex) {
                        //I don't care atm..
                    }
                }
            });
            valueAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);

                    // if (mMarker != null) {
                    // mMarker.remove();
                    // }
                    // mMarker = googleMap.addMarker(new MarkerOptions().position(endPosition).icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_car)));

                }
            });
            valueAnimator.start();
        }
    }
    private interface LatLngInterpolatorNew {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolatorNew {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }
    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }
}

class CustomRunnable implements Runnable{
    private Marker marcador;
    private TrackingActivity activity;
    private Handler handler;

    public CustomRunnable(Marker marker, TrackingActivity activity,Handler handler){
        this.marcador = marker;
        this.activity = activity;
        this.handler = handler;
    }

    @Override
    public void run() {
        //Alert.show(activity,"ubicacion",String.valueOf(marcador));
        activity.getLastProviderUbication(marcador);
        handler.postDelayed( new CustomRunnable(marcador,activity,handler) , 5000);
    }
}
