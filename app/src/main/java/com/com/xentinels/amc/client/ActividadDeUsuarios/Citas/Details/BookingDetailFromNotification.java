package com.com.xentinels.amc.client.ActividadDeUsuarios.Citas.Details;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.com.xentinels.amc.client.ActividadDeUsuarios.checkout.CheckoutActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import com.com.xentinels.amc.client.Internet.SolicitudesJson;
import com.com.xentinels.amc.client.Preferences;
import com.xentinels.amc.clients.R;
import com.com.xentinels.amc.client.VolleyRP;

public class BookingDetailFromNotification extends FragmentActivity implements OnMapReadyCallback {

    private String code;
    private boolean tracking;
    private boolean pagar;
    private GoogleMap mMap;
    private Double latitude;
    private Double longitude;
    private TextView nombreServicio;
    private TextView inicio;
    private TextView fin;
    private TextView valorAPagar;
    private TextView nombreMedico;
    private VolleyRP volley;
    private RequestQueue mRequest;
    private String cantidad;
    private String serviceName;
    private String total;
    private String transaction;
    private ProgressDialog pd;
    private Button btnCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_detail);
        Toolbar toolbar = findViewById(R.id.toolbarDetails);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        pd = new ProgressDialog(this);
        pd.setMessage("");
        pd.show();
        setTitle("Detalles de la cita");
        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        FloatingActionButton fab = findViewById(R.id.fab);
        volley = VolleyRP.getInstance(BookingDetailFromNotification.this);
        mRequest = volley.getRequestQueue();

        nombreServicio = findViewById(R.id.NombreServicio);
        inicio = findViewById(R.id.inicio);
        fin = findViewById(R.id.fin);
        valorAPagar = findViewById(R.id.valorAPagar);
        nombreMedico = findViewById(R.id.nombreMedico);
        btnCancel = findViewById(R.id.cancelButton);
        btnCancel.setVisibility(View.GONE);

        if (bundle != null) {
            code = bundle.getString("code");
            tracking = bundle.getBoolean("tracking");
            pagar = bundle.getBoolean("pagar");

            Log.d("DETAILS", String.valueOf(pagar));
            Log.d("DETAILS", String.valueOf(tracking));
            Log.d("DETAILS", code);
        }
        LoadDetails(code);

            fab.setImageResource(R.drawable.payment);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {///
                    AlertDialog.Builder myBuilder = new AlertDialog.Builder(BookingDetailFromNotification.this);
                    myBuilder.setMessage("Redirigiendo a la pantalla de pagos");
                    myBuilder.setPositiveButton("Ir", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            Intent i = new Intent(BookingDetailFromNotification.this, CheckoutActivity.class);
                            i.putExtra("cantidad", cantidad);
                            i.putExtra("serviceName",serviceName);
                            i.putExtra("total",total);
                            i.putExtra("transaction",transaction);
                            startActivity(i);

                        }
                    });
                    myBuilder.setCancelable(true);
                    myBuilder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }});
                    AlertDialog dialogDep = myBuilder.create();
                    dialogDep.show();


                }///
            });
    }
    private void LoadDetails(String code) {
        JsonObjectRequest solicitud = new JsonObjectRequest(Request.Method.GET, SolicitudesJson.URL_BOOKING_DETAILS+code,null, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject datos) {
                pd.dismiss();
                try {
                    JSONArray booking_detailsArray = datos.getJSONArray("booking_details");
                    for (int j = 0; j < booking_detailsArray.length(); j++) {
                        JSONObject jsonObjectSchedules = booking_detailsArray.getJSONObject(j);
                        cantidad = jsonObjectSchedules.getString("quantity");
                        total = jsonObjectSchedules.getString("total");
                        valorAPagar.setText("x"+cantidad+"        $"+total);
                        JSONObject serviceObj = jsonObjectSchedules.getJSONObject("service");
                        serviceName=serviceObj.getString("name");
                        nombreServicio.setText(serviceName);

                    }
                    if(datos.optJSONObject("provider")!=null){
                        JSONObject providerObject = datos.getJSONObject("provider");
                        nombreMedico.setText(providerObject.getString("first_name")+" "+providerObject.getString("last_name"));

                    }else{
                        nombreMedico.setText("Pendiente...");
                    }
                    if(datos.getBoolean("is_cancelable")){
                        btnCancel.setVisibility(View.VISIBLE);
                    }
                    latitude = datos.getDouble("latitude");
                    longitude = datos.getDouble("longitude");
                    transaction = datos.getString("transaction");
                    inicio.setText(datos.getString("estimated_start_booking").split("T")[0]+" - "+datos.getString("estimated_start_booking").split("T")[1].split("-")[0]);
                    fin.setText(datos.getString("estimated_end_booking").split("T")[0]+" - "+datos.getString("estimated_end_booking").split("T")[1].split("-")[0]);

                        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                                .findFragmentById(R.id.mapView);
                        mapFragment.getMapAsync(BookingDetailFromNotification.this);
                    } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String jsonError = "";
                if (networkResponse != null && networkResponse.data != null) {
                    jsonError = new String(networkResponse.data);
                    System.out.println(jsonError);
                    VolleyLog.d("Error:", jsonError);
                    Toast.makeText(BookingDetailFromNotification.this,jsonError, Toast.LENGTH_LONG).show();
                }
            }}){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization","Token "+ Preferences.obtenerPreferenceString(BookingDetailFromNotification.this,Preferences.PREFERENCE_SESSION_TOKEN));
                return headers;
            }
        };
        VolleyRP.addToQueue(solicitud,mRequest, BookingDetailFromNotification.this,volley);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng sydney = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Lugar de la cita médica"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        mMap.animateCamera( CameraUpdateFactory.newLatLngZoom( sydney, 16 ), 1000, null);
    }
    @Override
    protected void onPause() {
        super.onPause();
    }

}
