package com.com.xentinels.amc.client.ActividadDeUsuarios.checkout;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.paymentez.android.model.Card;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import com.com.xentinels.amc.client.ActividadDeUsuarios.Cards.Alert;
import com.com.xentinels.amc.client.ActividadDeUsuarios.Cards.BackendService;
import com.com.xentinels.amc.client.ActividadDeUsuarios.Cards.ListCardsActivity;
import com.com.xentinels.amc.client.ActividadDeUsuarios.Cards.RetrofitFactory;
import com.com.xentinels.amc.client.Internet.SolicitudesJson;
import com.com.xentinels.amc.client.Preferences;
import com.xentinels.amc.clients.R;
import com.com.xentinels.amc.client.VolleyRP;


public class CheckoutActivity extends AppCompatActivity {

    LinearLayout buttonSelectPayment;
    ImageView imageViewCCImage;
    TextView textViewCCLastFour;
    Button buttonPlaceOrder;
    Context mContext;
    String CARD_TOKEN = "";
    int SELECT_CARD_REQUEST = 1004;
    String ORDER_AMOUNT ;
    String quantity;
    String transaction;
    String serviceName;
    TextView Tquantity;
    TextView TserviceName;
    TextView valueindividual;
    TextView Ttotal;
    private VolleyRP volley;
    private RequestQueue mRequest;
    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        mContext = this;
        pd = new ProgressDialog(mContext);
        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        if (bundle != null) {
            ORDER_AMOUNT = bundle.getString("total");
            quantity = bundle.getString("cantidad");
            serviceName = bundle.getString("serviceName");
            transaction = bundle.getString("transaction");
        }
         Tquantity = (TextView) findViewById(R.id.quantity);
         TserviceName = (TextView) findViewById(R.id.serviceName);
         valueindividual = (TextView) findViewById(R.id.valueindividual);
         Ttotal = (TextView) findViewById(R.id.total);
         Tquantity.setText(quantity);
         TserviceName.setText(serviceName);
         Double individual = Double.valueOf(ORDER_AMOUNT)/Double.valueOf(quantity);
         valueindividual.setText(String.valueOf(individual));
         Ttotal.setText(ORDER_AMOUNT);
        final BackendService backendService = RetrofitFactory.getClient().create(BackendService.class);
        volley = VolleyRP.getInstance(this);
        mRequest = volley.getRequestQueue();

        imageViewCCImage = (ImageView) findViewById(R.id.imageViewCCImage);
        textViewCCLastFour = (TextView) findViewById(R.id.textViewCCLastFour);

        buttonPlaceOrder = (Button)findViewById(R.id.buttonPlaceOrder);
        buttonPlaceOrder.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(CARD_TOKEN == null || CARD_TOKEN.equals("")){
                    Alert.show(mContext,
                            "Error",
                            "Selecciona una tarjeta de crédito!");
                }else{

                    pd.setMessage("");
                    pd.show();


                    String ORDER_ID = ""+System.currentTimeMillis();
                    String ORDER_DESCRIPTION = "ORDER #" + ORDER_ID;
                    String DEV_REFERENCE = ORDER_ID;

                    generatePayment(transaction,CARD_TOKEN);


                }
            }
        });

        buttonSelectPayment = (LinearLayout)findViewById(R.id.buttonSelectPayment);
        buttonSelectPayment.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ListCardsActivity.class);
                startActivityForResult(intent, SELECT_CARD_REQUEST);
            }
        });
    }

    private void generatePayment(String transaction, String card_token) {
        Map<String, String> params = new HashMap();
        params.put("token", card_token);
        params.put("transaction", transaction);
        JSONObject paymentParams = new JSONObject(params);
        JsonObjectRequest solicitud = new JsonObjectRequest(Request.Method.POST, SolicitudesJson.URL_PAY_BOOKING,paymentParams, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject datos) {
                try {
                    JSONObject transactionObj = datos.getJSONObject("transaction");
                    pd.dismiss();
 /*                   Alert.show(mContext,
                            "Successful Charge",
                            "status: " + transactionObj.getString("status") +
                                    "\nstatus_detail: " + transactionObj.getString("status_detail") +
                                    "\nmessage: " + transactionObj.getString("message") +
                                    "\ntransaction_id:" + transactionObj.getString("id"));*/
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                    builder1.setTitle("Pago exitoso.");
                    builder1.setMessage("El pago se realizó correctamente, gracias!");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    finish();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String jsonError = "";
                if (networkResponse != null && networkResponse.data != null) {
                    jsonError = new String(networkResponse.data);
                    System.out.println(jsonError);
                    VolleyLog.d("Error:", jsonError);
                    Toast.makeText(CheckoutActivity.this,jsonError, Toast.LENGTH_LONG).show();
                    Toast.makeText(CheckoutActivity.this,"Ocurrio un error con el pago, intente de nuevo más tarde", Toast.LENGTH_LONG).show();
                }
            }}){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization","Token "+ Preferences.obtenerPreferenceString(CheckoutActivity.this,Preferences.PREFERENCE_SESSION_TOKEN));
                return headers;
            }
        };
        VolleyRP.addToQueue(solicitud,mRequest,CheckoutActivity.this,volley);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_CARD_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                CARD_TOKEN = data.getStringExtra("CARD_TOKEN");
                String CARD_TYPE = data.getStringExtra("CARD_TYPE");
                String CARD_LAST4 = data.getStringExtra("CARD_LAST4");

                if (CARD_LAST4 != null && !CARD_LAST4.equals("")) {
                    textViewCCLastFour.setText("XXXX." + CARD_LAST4);
                    imageViewCCImage.setImageResource(Card.getDrawableBrand(CARD_TYPE));
                }

            }
        }
    }
}
