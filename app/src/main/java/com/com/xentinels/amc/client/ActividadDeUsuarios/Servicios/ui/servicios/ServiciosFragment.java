package com.com.xentinels.amc.client.ActividadDeUsuarios.Servicios.ui.servicios;

import androidx.lifecycle.ViewModelProviders;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.com.xentinels.amc.client.Internet.SolicitudesJson;
import com.com.xentinels.amc.client.Preferences;
import com.xentinels.amc.clients.R;
import com.com.xentinels.amc.client.VolleyRP;

public class ServiciosFragment extends Fragment {

    private ServiciosViewModel mViewModel;
    Context thiscontext;
    private VolleyRP volley;
    private RequestQueue mRequest;
    private RecyclerView rv;
    private List<AtributosServicios> atributosList;
    private AdapterServicios adapter;
    private LinearLayout layoutVacio;
    private EventBus bus = EventBus.getDefault();
    private static String CATEGORY_SERVICE_ID;
    private ProgressDialog pd;

    public static ServiciosFragment newInstance(String CATEGORY_SERVICE) {
        CATEGORY_SERVICE_ID = CATEGORY_SERVICE;
        return new ServiciosFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        thiscontext = container.getContext();
        pd = new ProgressDialog(thiscontext);
        pd.setMessage("");
        pd.show();
        View v = inflater.inflate(R.layout.fragment_servicios, container, false);
        Toolbar toolbar = v.findViewById(R.id.toolbarServices);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        volley = VolleyRP.getInstance(getActivity());
        mRequest = volley.getRequestQueue();
        atributosList = new ArrayList<AtributosServicios>();

        rv = v.findViewById(R.id.serviciosRecyclerView);
        layoutVacio = v.findViewById(R.id.layoutVacioServicios);
        LinearLayoutManager lm = new LinearLayoutManager(getContext());
        rv.setLayoutManager(lm);
        adapter = new AdapterServicios(atributosList,getContext(),this);

        rv.setAdapter(adapter);
        cargarServicios();
        verificarSiExistenServicios();
        return v;

    }
    public void verificarSiExistenServicios(){
        if(atributosList.isEmpty()){
            layoutVacio.setVisibility(View.VISIBLE);
            rv.setVisibility(View.GONE);
        }else{
            layoutVacio.setVisibility(View.GONE);
            rv.setVisibility(View.VISIBLE);
        }
    }

    private void actualizarTarjetas(){
        adapter.notifyDataSetChanged();
        verificarSiExistenServicios();
    }

    public void agregarServicio(String urlFotoDeServicio, String nombre, String ultimoMensaje, String id,String type, Double value){
        AtributosServicios serviciosAtributos = new AtributosServicios();
        serviciosAtributos.setFoto(urlFotoDeServicio);
        serviciosAtributos.setNombreCompleto(nombre);
        serviciosAtributos.setDescripcion(ultimoMensaje);
        serviciosAtributos.setId(id);
        serviciosAtributos.setType_mensaje(type);
        serviciosAtributos.setValue(value);
        atributosList.add(serviciosAtributos);
        adapter.notifyDataSetChanged();
        verificarSiExistenServicios();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        adapter.onActivityResult(requestCode, resultCode, data);
    }

    public void agregarServicio(AtributosServicios a){
        atributosList.add(0,a);
        adapter.notifyDataSetChanged();
        verificarSiExistenServicios();
    }

    public void cargarServicios(){
        Map<String, String> params = new HashMap();
        params.put("category__code", CATEGORY_SERVICE_ID);
        JSONObject loginParams = new JSONObject(params);

        JsonObjectRequest solicitud = new JsonObjectRequest(Request.Method.GET, SolicitudesJson.URL_GET_SERVICES+"?category__code="+CATEGORY_SERVICE_ID,null, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject datos) {
                pd.dismiss();
                try {
                    String TodosLosDatos = datos.getString("results");
                    JSONArray jsonArray = new JSONArray(TodosLosDatos);
                    for(int i =0;i<jsonArray.length();i++) {
                        JSONObject jsonObject = new JSONObject(jsonArray.getString(i));
                        String code = jsonObject.getString("code");
                        String name = jsonObject.getString("name");
                        String description = jsonObject.getString("description");
                        String picture = jsonObject.getString("picture2");
                        Double value = jsonObject.getDouble("value");
                        agregarServicio(picture, name, description, code, "1",value);
                    }
                } catch (JSONException e) {
                    Toast.makeText(getContext(),"Ocurrio un error al descomponer el JSON",Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String jsonError = "";
                if (networkResponse != null && networkResponse.data != null) {
                    jsonError = new String(networkResponse.data);
                    System.out.println(jsonError);
                    VolleyLog.d("Error:", jsonError);
                    Toast.makeText(thiscontext,jsonError, Toast.LENGTH_LONG).show();
                }
            }}){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization","Token "+ Preferences.obtenerPreferenceString(thiscontext,Preferences.PREFERENCE_SESSION_TOKEN));
                return headers;
            }
        };
        VolleyRP.addToQueue(solicitud,mRequest,thiscontext,volley);
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void ejecutarLLamada(AtributosServicios a){
        agregarServicio(a);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void limpiarLista(){
        atributosList.clear();
        actualizarTarjetas();
    }

    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(ServiciosViewModel.class);
        // TODO: Use the ViewModel
    }
}
