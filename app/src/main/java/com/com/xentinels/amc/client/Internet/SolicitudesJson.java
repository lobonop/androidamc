package com.com.xentinels.amc.client.Internet;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;

import com.com.xentinels.amc.client.VolleyRP;


public abstract class SolicitudesJson {
    public final static String URL_REGISTER_TO_PUSH_NOTIFICATIONS = "https://auxiliomedicoencasa.com/api/v1/push_notifications/register-device/";

    public final static String URL_LOGIN = "https://auxiliomedicoencasa.com/api/v1/accounts/login/";
    public final static String URL_SINGUP = "https://auxiliomedicoencasa.com/api/v1/accounts/sign-up/";
    public final static String URL_GET_ALL_CATEGORIES = "https://auxiliomedicoencasa.com/api/v1/providers/categories/";
    public final static String URL_GET_SERVICES = "https://auxiliomedicoencasa.com/api/v1/providers/services/";
    public final static String URL_VERIFY = "https://auxiliomedicoencasa.com/api/v1/accounts/verify/";
    public final static String URL_GET_DOCTORS = "https://auxiliomedicoencasa.com/api/v1/providers/service-providers/";

    public final static String URL_PAYMENTS = "https://auxiliomedicoencasa.com/api/v1/payments/";
    public final static String URL_NEW_BOOKING = "https://auxiliomedicoencasa.com/api/v1/booking/start-new-booking/";
    public final static String URL_GET_LAST_BOOKING_CUSTOMER = "https://auxiliomedicoencasa.com/api/v1/booking/last-booking-customer/";
    public final static String URL_BOOKING_DETAILS = "https://auxiliomedicoencasa.com/api/v1/booking/booking-detail/";
    public final static String URL_PAY_BOOKING = "https://auxiliomedicoencasa.com/api/v1/payments/pay/";
    public final static String URL_AVAILABLE_SCHEDULES = "https://auxiliomedicoencasa.com/api/v1/booking/available-schedules/";
    public final static String URL_GET_PROVIDER_LOCATION = "https://auxiliomedicoencasa.com/api/v1/booking/provider-location/?booking=";
    public final static String URL_PUBLIC_CREDENTIALS = "https://auxiliomedicoencasa.com/api/v1/payments/public-credentials/";
    public abstract void solicitudCompletada(JSONObject j);
    public abstract void solicitudErronea();

    public SolicitudesJson(){}

    public void solicitudJsonGET(Context c,String URL){
        JsonObjectRequest solicitud = new JsonObjectRequest(URL,null, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject datos) {
                solicitudCompletada(datos);
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                solicitudErronea();
            }
        });
        VolleyRP.addToQueue(solicitud,VolleyRP.getInstance(c).getRequestQueue(),c,VolleyRP.getInstance(c));
    }

    public void solicitudJsonPOST(Context c, String URL, HashMap h){
        JsonObjectRequest solicitud = new JsonObjectRequest(Request.Method.POST,URL,new JSONObject(h), new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject datos) {
                solicitudCompletada(datos);
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                solicitudErronea();
            }
        });
        VolleyRP.addToQueue(solicitud,VolleyRP.getInstance(c).getRequestQueue(),c,VolleyRP.getInstance(c));
    }

}
