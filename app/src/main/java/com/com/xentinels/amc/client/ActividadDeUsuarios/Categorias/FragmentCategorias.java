package com.com.xentinels.amc.client.ActividadDeUsuarios.Categorias;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.com.xentinels.amc.client.Internet.SolicitudesJson;
import com.com.xentinels.amc.client.Preferences;
import com.com.xentinels.amc.client.VolleyRP;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.xentinels.amc.clients.R;

/**
 * Created by user on 8/05/2017.
 */

public class FragmentCategorias extends Fragment {
    Context thiscontext;
    private VolleyRP volley;
    private RequestQueue mRequest;
    private RecyclerView rv;
    private List<AtributosCategorias> atributosList;
    private AdapterCategorias adapter;
    private LinearLayout layoutVacio;
    private EventBus bus = EventBus.getDefault();
    private ProgressDialog pd;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        thiscontext = container.getContext();
        pd = new ProgressDialog(thiscontext);
        pd.setMessage("");
        pd.show();
        View v = inflater.inflate(R.layout.fragment_categorias,container,false);
        volley = VolleyRP.getInstance(getActivity());
        mRequest = volley.getRequestQueue();
        atributosList = new ArrayList<>();

        pd.setMessage("");
        pd.show();
        rv = v.findViewById(R.id.categoriasRecyclerView);
        layoutVacio = v.findViewById(R.id.layoutVacio);
        LinearLayoutManager lm = new LinearLayoutManager(getContext());
        rv.setLayoutManager(lm);

        adapter = new AdapterCategorias(atributosList,getContext(),this);
        rv.setAdapter(adapter);
        return v;
    }

    public void verificarSiExistenCategorias(){
        if(atributosList.isEmpty()){
            layoutVacio.setVisibility(View.VISIBLE);
            rv.setVisibility(View.GONE);
        }else{
            layoutVacio.setVisibility(View.GONE);
            rv.setVisibility(View.VISIBLE);
        }
    }

    private void actualizarTarjetas(){
        adapter.notifyDataSetChanged();
        verificarSiExistenCategorias();
    }

    //id
    //nombre
    //ultimo_mensaje
    //hora
    public void agregarCategoria(String urlFotoDeServicio, String nombre, String ultimoMensaje, String id, String type){
        AtributosCategorias amigosAtributos = new AtributosCategorias();
        amigosAtributos.setFoto(urlFotoDeServicio);
        amigosAtributos.setNombreCompleto(nombre);
        amigosAtributos.setDescripcion(ultimoMensaje);
        amigosAtributos.setId(id);
        amigosAtributos.setType_mensaje(type);
        atributosList.add(amigosAtributos);
        adapter.notifyDataSetChanged();
        verificarSiExistenCategorias();
    }

    public void agregarCategoria(AtributosCategorias a){
        atributosList.add(0,a);
        adapter.notifyDataSetChanged();
        verificarSiExistenCategorias();
    }

    public void cargarCategoria(){
        JsonObjectRequest solicitud = new JsonObjectRequest(Request.Method.GET, SolicitudesJson.URL_GET_ALL_CATEGORIES,null, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject datos) {
                pd.dismiss();
                try {
                    String TodosLosDatos = datos.getString("results");
                    JSONArray jsonArray = new JSONArray(TodosLosDatos);
                    for(int i =0;i<jsonArray.length();i++) {
                    JSONObject jsonObject = new JSONObject(jsonArray.getString(i));
                    String code = jsonObject.getString("code");
                    String name = jsonObject.getString("name");
                    String description = jsonObject.getString("description");
                    String picture = jsonObject.getString("picture2");
                    agregarCategoria(picture, name, description, code, "1");
                    }
                    } catch (JSONException e) {
                    Toast.makeText(getContext(),"Ocurrio un error al descomponer el JSON",Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String jsonError = "";
                if (networkResponse != null && networkResponse.data != null) {
                    jsonError = new String(networkResponse.data);
                    System.out.println(jsonError);
                    VolleyLog.d("Error:", jsonError);
                    Toast.makeText(thiscontext,jsonError, Toast.LENGTH_LONG).show();
                }
            }}){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization","Token "+ Preferences.obtenerPreferenceString(thiscontext,Preferences.PREFERENCE_SESSION_TOKEN));
                return headers;
            }
        };
        VolleyRP.addToQueue(solicitud,mRequest,thiscontext,volley);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void ejecutarLLamada(AtributosCategorias a){
        agregarCategoria(a);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void limpiarLista(){
        atributosList.clear();
        Toast.makeText(getContext(),"recargando... Categorías",Toast.LENGTH_LONG).show();
      //  actualizarTarjetas();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
                limpiarLista();
                cargarCategoria();
                verificarSiExistenCategorias();

    }
}
