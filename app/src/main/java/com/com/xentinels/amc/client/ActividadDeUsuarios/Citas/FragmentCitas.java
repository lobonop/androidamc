package com.com.xentinels.amc.client.ActividadDeUsuarios.Citas;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.com.xentinels.amc.client.Internet.SolicitudesJson;
import com.com.xentinels.amc.client.Preferences;
import com.xentinels.amc.clients.R;
import com.com.xentinels.amc.client.VolleyRP;


public class FragmentCitas extends Fragment {

    private RecyclerView rv;
    private CitasAdapter adapter;
    private List<Citas> listCitas;
    private LinearLayout layoutSinCitas;
    private VolleyRP volley;
    private RequestQueue mRequest;
    Context thiscontext;


    private EventBus bus = EventBus.getDefault();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_citas,container,false);
        volley = VolleyRP.getInstance(getActivity());
        mRequest = volley.getRequestQueue();
        listCitas = new ArrayList<>();
        thiscontext = container.getContext();

        rv = v.findViewById(R.id.citasRecyclerView);
        layoutSinCitas = v.findViewById(R.id.layoutVacioCitas);
        LinearLayoutManager lm = new LinearLayoutManager(thiscontext);
        rv.setLayoutManager(lm);

        adapter = new CitasAdapter(listCitas,thiscontext,this);
        rv.setAdapter(adapter);


        return v;
    }

    private void cargarCitas() {
        JsonObjectRequest solicitud = new JsonObjectRequest(Request.Method.GET, SolicitudesJson.URL_GET_LAST_BOOKING_CUSTOMER,null, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject datos) {

                try {
                    String results = datos.getString("results");
                    JSONArray resultsArray = new JSONArray(results);
                    for(int i =0;i<resultsArray.length();i++) {
                        JSONObject jsonObject = new JSONObject(resultsArray.getString(i));
                        String code = jsonObject.getString("code");
                        String nombreServicio = "Servicio médico";
                        String estado = jsonObject.getString("status_booking");
                        String total = jsonObject.getString("booking_total");
                        String horaInicio = jsonObject.getString("estimated_start_booking").split("T")[1].split("-")[0];
                        String horaFin = jsonObject.getString("estimated_end_booking").split("T")[1].split("-")[0];
                        JSONArray booking_detailsArray = jsonObject.getJSONArray("booking_details");
                        for (int j = 0; j < booking_detailsArray.length(); j++) {
                            JSONObject jsonObjectService = booking_detailsArray.getJSONObject(j);
                            nombreServicio = jsonObjectService.getString("service");
                        }


                        agregarCita(code,  nombreServicio,  estado,  total,  horaInicio,  horaFin);
                    }
                } catch (JSONException e) {
                    Toast.makeText(thiscontext,"Ocurrió un error interno, intente de nuevo",Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String jsonError = "";
                if (networkResponse != null && networkResponse.data != null) {
                    jsonError = new String(networkResponse.data);
                    System.out.println(jsonError);
                    VolleyLog.d("Error:", jsonError);
                    Toast.makeText(thiscontext,jsonError, Toast.LENGTH_LONG).show();
                }
            }}){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization","Token "+ Preferences.obtenerPreferenceString(thiscontext,Preferences.PREFERENCE_SESSION_TOKEN));
                return headers;
            }
        };
        VolleyRP.addToQueue(solicitud,mRequest,thiscontext,volley);
    }

    public void verificarSiTenemosCitas(){
        if(listCitas.isEmpty()){
            layoutSinCitas.setVisibility(View.VISIBLE);
            rv.setVisibility(View.GONE);
        }else{
            layoutSinCitas.setVisibility(View.GONE);
            rv.setVisibility(View.VISIBLE);
        }
    }


    public void agregarCita(String code, String nombreServicio, String estado, String total, String horaInicio, String horaFin){
        Citas cita = new Citas( code,  nombreServicio,  estado,  total,  horaInicio,  horaFin);
        listCitas.add(cita);
        adapter.notifyDataSetChanged();
        verificarSiTenemosCitas();

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void limpiarLista() {
        listCitas.clear();
    }

    @Override
    public void onResume() {
        super.onResume();
        limpiarLista();
        cargarCitas();
        verificarSiTenemosCitas();
    }

}
