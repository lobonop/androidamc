package com.com.xentinels.amc.client;

import android.content.Context;
import android.content.SharedPreferences;


public class Preferences {

    public static final String STRING_PREFERENCES = "michattimereal.Mensajes.Mensajeria";
    public static final String PREFERENCE_SESSION_KEEP_LOGGED_IN = "SESSION.KEEP_LOGED_IN";
    public static final String PREFERENCE_SESSION_TOKEN = "SESSION.TOKEN";
    public static final String PREFERENCE_USUARIO_LOGIN = "usuario.login";
    public static final String PREFERENCE_ERROR_FIELD = "non_field_errors";
    public static final String IS_VERIFIED = "usuario.isVerified";
    public static final String PREFERENCE_USER_PHONE = "usuario.phone";
    public static final String PREFERENCE_SESSION_ID_USER ="usuario.id";
    public static final String PREFERENCE_SESSION_EMAIL_USER ="usuario.email";
    public static final String PAYMENTEZ_IS_TEST_MODE = "enviroment";
    public static final String PAYMENTEZ_CLIENT_APP_CODE ="client.app.code";
    public static final String PAYMENTEZ_CLIENT_APP_KEY ="client.app.key";


    public static void savePreferenceBoolean(Context c, boolean b,String key){
        SharedPreferences preferences = c.getSharedPreferences(STRING_PREFERENCES, Context.MODE_PRIVATE);
        preferences.edit().putBoolean(key,b).apply();
    }

    public static void savePreferenceString(Context c, String b, String key){
        SharedPreferences preferences = c.getSharedPreferences(STRING_PREFERENCES, Context.MODE_PRIVATE);
        preferences.edit().putString(key,b).apply();
    }

    public static boolean obtenerPreferenceBoolean(Context c,String key){
        SharedPreferences preferences = c.getSharedPreferences(STRING_PREFERENCES, Context.MODE_PRIVATE);
        return preferences.getBoolean(key,false);//Si es que nunca se ha guardado nada en esta key pues retornara false
    }

    public static String obtenerPreferenceString(Context c,String key){
        SharedPreferences preferences = c.getSharedPreferences(STRING_PREFERENCES, Context.MODE_PRIVATE);
        return preferences.getString(key,"");//Si es que nunca se ha guardado nada en esta key pues retornara una cadena vacia
    }

}
