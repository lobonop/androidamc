package com.com.xentinels.amc.client.ActividadDeUsuarios.Doctores.Servicios.ui.doctores;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.shivtechs.maplocationpicker.LocationPickerActivity;
import com.shivtechs.maplocationpicker.MapUtility;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.com.xentinels.amc.client.ActividadDeUsuarios.ActivityUsuarios;
import com.com.xentinels.amc.client.ActividadDeUsuarios.Cards.Alert;
import com.com.xentinels.amc.client.Internet.SolicitudesJson;
import com.com.xentinels.amc.client.Preferences;
import com.xentinels.amc.clients.R;
import com.com.xentinels.amc.client.VolleyRP;

import static com.com.xentinels.amc.client.ActividadDeUsuarios.Doctores.Servicios.ui.doctores.DoctoresFragment.serviceId;


public class AdapterDoctores extends RecyclerView.Adapter<AdapterDoctores.HolderDoctores> implements DatePickerDialog.OnDateSetListener {

    private VolleyRP volley;
    private RequestQueue mRequest;
    private List<AtributosDoctores> atributosList;
    private Context context;
    private DoctoresFragment f;
    private int quantity;
    private String txtLatLong;
    private String txtAddress;
    private static final int ADDRESS_PICKER_REQUEST = 1020;
    private JSONObject mainJson;
    private JSONObject detailsJson;
    private JSONArray infoDetailsJson;




    public AdapterDoctores(List<AtributosDoctores> atributosList, Context context, DoctoresFragment f){
        this.atributosList = atributosList;
        this.context = context;
        this.f=f;
        volley = VolleyRP.getInstance(context);
        mRequest = volley.getRequestQueue();
    }

    @Override
    public HolderDoctores onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_doctores,parent,false);
        return new AdapterDoctores.HolderDoctores(v);
    }

    @Override
    public void onBindViewHolder(final HolderDoctores holder, final int position) {
        Picasso.get().load(atributosList.get(position).getImageURL()).error(R.drawable.ic_account_circle).into(holder.imageView);
        holder.nombre.setText("  " +atributosList.get(position).getName()+" "+atributosList.get(position).getLastname());
        holder.carrera.setText(atributosList.get(position).getCareer_name());
        holder.college.setText(atributosList.get(position).getCollege_name());
        int id = 0;

        holder.agendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder b = new AlertDialog.Builder(context);
                b.setTitle("Horarios Disponibles");
                HashMap<String, String> schedule = atributosList.get(position).getSchedules();

                Integer id = 0;
                String desde;
                String hasta;
                HashMap<String, String> temporal = new HashMap<String, String>();

                HashMap<Integer, HashMap<String, String>> scheduleId = new HashMap<Integer, HashMap<String, String>>();
                HashMap<Integer,String> horarios = new HashMap<Integer,String>();

                for(Map.Entry<String, String> entry : schedule.entrySet()) {
                    desde = entry.getKey();
                    hasta = entry.getValue();
                    temporal.put("desde",desde);
                    temporal.put("hasta",hasta);
                    scheduleId.put(id,temporal);
                    horarios.put(id,desde.split("T")[1].split("-")[0]+"-"+hasta.split("T")[1].split("-")[0]);
                    id++;
                    temporal = new HashMap<String, String>();
                }
                final HashMap<Integer, HashMap<String, String>> scheduleIdToshow = scheduleId;
                String[] types = new String[horarios.size()];
                int index = 0;
                for (Map.Entry<Integer, String> mapEntry : horarios.entrySet()) {
                    types[index] = mapEntry.getValue();
                    index++;
                }
                b.setItems(types, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialogint, int which) {//////////////

                        dialogint.dismiss();
                        String desde = scheduleIdToshow.get(which).get("desde");
                        String hasta = scheduleIdToshow.get(which).get("hasta");
                        AlertDialog.Builder myBuilder = new AlertDialog.Builder(context);
                        myBuilder.setMessage("Está seguro que desea agendar una cita en el horario seleccionado?");
                        myBuilder.setTitle("Confirmación");
                        myBuilder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                NumberPicker picker = new NumberPicker(context);
                                picker.setMinValue(1);
                                picker.setMaxValue(3);

                                FrameLayout layout = new FrameLayout(context);
                                layout.addView(picker, new FrameLayout.LayoutParams(
                                        FrameLayout.LayoutParams.WRAP_CONTENT,
                                        FrameLayout.LayoutParams.WRAP_CONTENT,
                                        Gravity.CENTER));

                                new AlertDialog.Builder(context)
                                        .setView(layout)
                                        .setTitle("Escoja la cantidad de personas que usaran el servicio")
                                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                quantity = picker.getValue();
                                                mainJson  = new JSONObject();
                                                infoDetailsJson  = new JSONArray();
                                                detailsJson  = new JSONObject();
                                                try {
                                                    detailsJson.put("service", serviceId);
                                                    detailsJson.put("quantity", quantity);
                                                    infoDetailsJson.put(detailsJson);
                                                    mainJson.put("booking_details", infoDetailsJson);
                                                    mainJson.put("estimated_start_booking", desde);
                                                    mainJson.put("estimated_end_booking", hasta);
                                                    mainJson.put("provider", atributosList.get(position).getId());
                                                    mainJson.put("customer", Preferences.obtenerPreferenceString(context,Preferences.PREFERENCE_SESSION_ID_USER));
                                                    Alert.show(context,"Ubicación","Selecciona el lugar donde se realizará la cita");
                                                    Intent i = new Intent(context, LocationPickerActivity.class);
                                                    ((Activity)context).startActivityForResult(i,ADDRESS_PICKER_REQUEST);
                                                } catch ( JSONException e ) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        })
                                        .setNegativeButton(android.R.string.cancel, null)
                                        .show();
                            }
                            });
                        myBuilder.setCancelable(true);
                        myBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }

                        });
                        AlertDialog dialog = myBuilder.create();
                        dialog.show();

                    }////////////

                });

                b.show();



            }
        }

        );
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });
    }

    @Override
    public int getItemCount() {
        return atributosList.size();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ADDRESS_PICKER_REQUEST) {
            try {
                if (data != null && data.getStringExtra(MapUtility.ADDRESS) != null) {
                    String address = data.getStringExtra(MapUtility.ADDRESS);
                    double selectedLatitude = round(data.getDoubleExtra(MapUtility.LATITUDE, 0.0),6);
                    double selectedLongitude = round(data.getDoubleExtra(MapUtility.LONGITUDE, 0.0),6);
                    txtLatLong = "Lat:"+selectedLatitude+"  Long:"+selectedLongitude;
                    Toast.makeText(context,txtLatLong,Toast.LENGTH_LONG).show();
                    AgendarCita(selectedLatitude,selectedLongitude,mainJson);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    private void AgendarCita(double selectedLatitude, double selectedLongitude, JSONObject mainJson) {
    try{
        mainJson.put("latitude",selectedLatitude);
        mainJson.put("longitude",selectedLongitude);
        JsonObjectRequest solicitud = new JsonObjectRequest(Request.Method.POST, SolicitudesJson.URL_NEW_BOOKING,mainJson, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject datos) {
                    Alert.show(context,"Correcto","La cita se agendó correctamente, espere que sea aceptada para pagarla");
                    Alert.show(context,"Información","Puede ver el estado de su cita en CITAS");

                Intent i = new Intent(context, ActivityUsuarios.class);
                context.startActivity(i);
                ((Activity)context).finish();
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String jsonError = "";
                if (networkResponse != null && networkResponse.data != null) {
                    jsonError = new String(networkResponse.data);
                    System.out.println(jsonError);
                    VolleyLog.d("Error:", jsonError);
                    Toast.makeText(context,jsonError, Toast.LENGTH_LONG).show();
                }
            }}){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization","Token "+ Preferences.obtenerPreferenceString(context,Preferences.PREFERENCE_SESSION_TOKEN));
                return headers;
            }
        };
        VolleyRP.addToQueue(solicitud,mRequest,context,volley);











    } catch ( JSONException e ) {
        e.printStackTrace();
    }


    }

    static class HolderDoctores extends RecyclerView.ViewHolder{

        CardView cardView;
        ImageView imageView;
        TextView nombre;
        TextView carrera;
        TextView college;
        Button agendar;


        public HolderDoctores(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cardViewDoctores);
            imageView = itemView.findViewById(R.id.fotoDoctor);
            nombre = itemView.findViewById(R.id.nombreDoctor);
            carrera = itemView.findViewById(R.id.carrera);
            college = itemView.findViewById(R.id.college);
            agendar = itemView.findViewById(R.id.buttonAgendar);
        }
    }

}
