package com.com.xentinels.amc.client.ActividadDeUsuarios.Cards;

import com.com.xentinels.amc.client.ActividadDeUsuarios.Cards.model.CreateChargeResponse;
import com.com.xentinels.amc.client.ActividadDeUsuarios.Cards.model.DeleteCardResponse;
import com.com.xentinels.amc.client.ActividadDeUsuarios.Cards.model.GetCardsResponse;
import com.com.xentinels.amc.client.ActividadDeUsuarios.Cards.model.VerifyResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface BackendService {
    @GET("cards/")
    Call<GetCardsResponse> getCards(@Header("Authorization") String token);

    @FormUrlEncoded
    @POST("/create-charge")
    Call<CreateChargeResponse> createCharge(@Field("uid") String uid, @Field("session_id") String session_id,
                                            @Field("token") String token, @Field("amount") double amount,
                                            @Field("dev_reference") String dev_reference, @Field("description") String description);
    @FormUrlEncoded
    @POST("delete-card/")
    Call<DeleteCardResponse> deleteCard(@Header("Authorization") String autToken, @Field("token") String token);

    @FormUrlEncoded
    @POST("/verify-transaction")
    Call<VerifyResponse> verifyTransaction(@Field("uid") String uid, @Field("transaction_id") String transaction_id,
                                           @Field("type") String type, @Field("value") String value);
}
